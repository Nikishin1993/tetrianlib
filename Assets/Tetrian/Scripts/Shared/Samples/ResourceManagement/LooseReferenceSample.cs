﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Core.ResourceManagement.Samples
{
    [CreateAssetMenu(fileName = @"LooseReferenceSample", menuName = @"Tetrian/Samples/Loose Reference Sample", order = 0)]
    public class LooseReferenceSample : ScriptableObject
    {
        [SerializeField] private List<LooseReference<Texture2D>> _textureAssetsList = null;
        [SerializeField] private LooseReference<Texture2D> _textureAsset = null;
    }
}
