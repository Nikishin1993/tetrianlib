﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections.Generic;
using Tetrian.Core.Composition;
using UnityEngine;

namespace Tetrian.Core.ResourceManagement.Samples
{
    public class RuntimeLooseReferenceSample : MonoBehaviour
    {
        [SerializeField] private LooseReference<Texture2D> _looseTextureReference = null;
        [SerializeField] private MeshRenderer _targetObject = null;

        private void Update()
        {
            if (_targetObject == null)
                return;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (_looseTextureReference.IsAssetLoaded)
                    _looseTextureReference.UnloadAsset();
                else
                    _targetObject.material.mainTexture = _looseTextureReference.LoadAsset();
            }
        }

    }

}
