﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tetrian.Utility
{
    public static class ListExtensions
    {
        public static List<TR> OfType<TR>(this IList list)
        {
            List<TR> result = new List<TR>(list.Count);
            
            foreach (object item in list)
            {
                if (item is TR resultItem)
                    result.Add(resultItem);
            }

            return result;
        }
    }
}