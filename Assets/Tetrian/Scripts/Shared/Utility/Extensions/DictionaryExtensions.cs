﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;

namespace Tetrian.Utility
{
    public static class DictionaryExtensions
    {
        public static Dictionary<TK, TV> FillWithKeys<TK, TV>(this Dictionary<TK, TV> dictionary, Func<TK, TV> createValueDelegate)
            where TK : Enum
        {
            Array keys = Enum.GetValues(typeof(TK));

            foreach (object key in keys)
            {
                TK enumKey = (TK)key;
                dictionary.Add(enumKey, createValueDelegate.Invoke(enumKey));
            }

            return dictionary;
        }
        
        public static TV GetOrCreate<TK, TV>(this Dictionary<TK, TV> dictionary, TK key, Func<TV> createValueDelegate)
        {
            TV result;

            if (dictionary.TryGetValue(key, out result))
                return result;

            result = createValueDelegate.Invoke();
            dictionary.Add(key, result);
            return result;
        }

        public static TV GetOrCreate<TK, TV>(this Dictionary<TK, TV> dictionary, TK key)
            where TV : new()
        {
            TV result;

            if (dictionary.TryGetValue(key, out result))
                return result;

            result = new TV();
            dictionary.Add(key, result);
            return result;
        }

        public static TV GetOrDefault<TK, TV>(this Dictionary<TK, TV> dictionary, TK key, TV defaultValue = default)
        {
            if (dictionary.TryGetValue(key, out TV result))
                return result;

            return defaultValue;
        }
    }
}