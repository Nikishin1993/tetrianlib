﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using UnityEngine;

namespace Tetrian.Utility
{
    public static class VectorMasks
    {
        #region Vector3 to Vector3
        public static Vector3 XY0(this Vector3 v3) { return new Vector3(v3.x, v3.y, 0f); }
        public static Vector3 X0Z(this Vector3 v3) { return new Vector3(v3.x, 0f, v3.z); }
        #endregion

        #region Vector3 to Vector2
        public static Vector2 XZ(this Vector3 v3) { return new Vector2(v3.x, v3.z); }
        #endregion

        #region Vector2 to Vector3
        public static Vector2 X0Y(this Vector2 v2) { return new Vector3(v2.x, 0f, v2.y); }
        #endregion
    }
}
