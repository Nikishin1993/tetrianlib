﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using UnityEngine;

namespace Tetrian.Utility
{
    public static class InspectorLayout
    {
        public static Rect HorizontalLayout(ref Rect position, float width, float spacing)
        {
            Rect result = new Rect(position.x, position.y, width, position.height);
            position.x = position.x + width + spacing;
            position.width = position.width - width - spacing;
            return result;
        }

        public static Rect VerticalLayout(ref Rect position, float height, float spacing)
        {
            Rect result = new Rect(position.x, position.y, position.width, height);
            position.y = position.y + height + spacing;
            position.height = position.height - height - spacing;
            return result;
        }
    }
}