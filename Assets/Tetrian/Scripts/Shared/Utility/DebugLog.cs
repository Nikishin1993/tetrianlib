﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Utility
{
    public static class DebugLog
    {
        private const string NULL_MESSAGE = @"NULL";

        public static void Error(object source, object message)
        {
            Log(LogType.Error, source?.GetType(), message, null);
        }

        private static void Log(LogType logType, Type sourceType, object message, UnityEngine.Object context)
        {
            string stringMessage;

            if (sourceType == null)
                stringMessage = GetString(message);
            else
                stringMessage = $"[{sourceType.Name}] {message}";

            Debug.unityLogger.Log(logType: logType, message: stringMessage, context: context);
        }

        private static string GetString(object message)
        {
            if (message == null)
                return NULL_MESSAGE;

            return message.ToString();
        }
    }
}