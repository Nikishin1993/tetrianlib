// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Systems.TextParticles.Samples
{
    public class TextParticleEmitter : MonoBehaviour
    {
        [Serializable]
        public class Element
        {
            public char Prefix = '_';
            public Color Color = Color.white;
        }

        [SerializeField] private ParticleSystem _textEmitter = null;
        [SerializeField] private List<string> _atlasMap = null;
        [SerializeField] private int _atlasColumns = 0;
        [SerializeField] private int _atlasRows = 0;
        [SerializeField] private int _minValue = 0;
        [SerializeField] private int _maxValue = 99999;
        [SerializeField] private float _emitInterval = 0.5f;

        [Header(@"Elements")]
        [SerializeField] private float _elementChance = 0.75f;
        [SerializeField] private Element _defaultElement = null;
        [SerializeField] private List<Element> _elements = null;

        private Dictionary<char, int> _characterMap = null;
        private float _lastEmitTime = float.MinValue;

        // Start is called before the first frame update
        void Start()
        {
            _characterMap = ParticleText.PackCharacterMap(_atlasMap, _atlasColumns, _atlasRows);
        }

        // Update is called once per frame
        void Update()
        {
            if ((Time.time - _lastEmitTime) > _emitInterval)
            {
                _lastEmitTime = Time.time;

                string text = UnityEngine.Random.Range(_minValue, _maxValue).ToString();

                if (UnityEngine.Random.Range(0f, 1f) > _elementChance)
                {
                    ParticleText.Emit(_textEmitter, _characterMap, _textEmitter.transform.position, text, new ParticleSystem.EmitParams() { startColor = _defaultElement.Color });
                }
                else
                {
                    Element element = _elements[UnityEngine.Random.Range(0, _elements.Count)];
                    ParticleText.Emit(_textEmitter, _characterMap, _textEmitter.transform.position, element.Prefix + text, new ParticleSystem.EmitParams() { startColor = element.Color });
                }
            }
        }
    }
}