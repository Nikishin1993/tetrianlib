// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Systems.TextParticles
{
    public class ParticleText : MonoBehaviour
    {
        private static int[] _data = new int[8];
        private static List<Vector4> _customData = new List<Vector4>();

        public static Dictionary<char, int> PackCharacterMap(List<string> characterMap, int columns, int rows)
        {
            Dictionary<char, int> result = new Dictionary<char, int>();

            for (int y = 0; y < Mathf.Min(characterMap.Count, rows); y++)
            {
                string row = characterMap[y];

                for (int x = 0; x < Mathf.Min(row.Length, columns); x++)
                {
                    char character = row[x];

                    if (result.ContainsKey(character))
                    {
                        Debug.LogError($"Duplicate character: {character}");
                        continue;
                    }

                    result.Add(character, x + (y * columns));
                }
            }

            return result;
        }

        public static void Emit(ParticleSystem emitter, Dictionary<char, int> map, Vector3 position, string text, ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams())
        {
            for (int i = 0; i < Mathf.Min(text.Length, 8); i++)
            {
                char character = text[i];

                if (!map.TryGetValue(character, out int index))
                    index = 0; //TODO: Fallback

                _data[i] = index;
            }

            _data[7] = text.Length;

            EncodeToVectors(_data, out Vector4 custom1, out Vector4 custom2);

            Vector3 startSize3D = emitParams.startSize3D;

            if (startSize3D == Vector3.zero)
            {
                if (emitter.main.startSize3D)
                {
                    startSize3D = new Vector3(
                        emitter.main.startSizeX.constant,
                        emitter.main.startSizeY.constant,
                        emitter.main.startSizeZ.constant);
                }
                else
                {
                    startSize3D = new Vector3(
                        emitter.main.startSize.constant,
                        emitter.main.startSize.constant,
                        emitter.main.startSize.constant);
                }
            }

            startSize3D.x = startSize3D.x * text.Length;
            emitParams.startSize3D = startSize3D;

            emitter.transform.position = position;
            emitter.Emit(emitParams, 1);

            //if (emitter.main.maxParticles > _customData.Capacity)
            //    _customData.Capacity = emitter.main.maxParticles;

            //�������� ����� ParticleSystemCustomData.Custom1 �� ParticleSystem
            emitter.GetCustomParticleData(_customData, ParticleSystemCustomData.Custom1);
            //������ ������ ���������� �������, �.�. ��� �������, ������� �� ������ ��� �������
            _customData[_customData.Count - 1] = custom1;
            //���������� ������ � ParticleSystem
            emitter.SetCustomParticleData(_customData, ParticleSystemCustomData.Custom1);

            //���������� ��� ParticleSystemCustomData.Custom2
            emitter.GetCustomParticleData(_customData, ParticleSystemCustomData.Custom2);
            _customData[_customData.Count - 1] = custom2;
            emitter.SetCustomParticleData(_customData, ParticleSystemCustomData.Custom2);
        }

        private static void EncodeToVectors(int[] data, out Vector4 v1, out Vector4 v2)
        {
            v1 = new Vector4(data[0], data[1], data[2], data[3]);
            v2 = new Vector4(data[4], data[5], data[6], data[7]);
        }
    }
}