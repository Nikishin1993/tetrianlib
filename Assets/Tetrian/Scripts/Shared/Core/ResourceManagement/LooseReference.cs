﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using UnityEngine;

namespace Tetrian.Core.ResourceManagement
{
    [Serializable]
    public abstract class LooseReference
    {
        [SerializeField] private string _unityInspectorLabelFix = null; //Fixes naming of elements in array. Has no other purpos and should always be null
        [SerializeField] protected string _guid = null;
        [SerializeField] protected string _path = null;
        [SerializeField] protected bool _isValidReference = false;
        [SerializeField] protected string _serializationMessage = null;

        public abstract Type GetAssetType();
    }

    [Serializable]
    public class LooseReference<T> : LooseReference
#if UNITY_EDITOR
        , ISerializationCallbackReceiver
#endif
        where T : UnityEngine.Object
    {
        public bool IsAssetLoaded { get; private set; } = false;
        public T Asset { get; private set; } = null;

        public override Type GetAssetType() { return typeof(T); }

        public T LoadAsset()
        {
            if (IsAssetLoaded)
                return Asset;

            if (!_isValidReference)
            {
                Debug.LogError($"[{nameof(LooseReference)}<{typeof(T).Name}>] Asset {_path ?? @"NULL"} {{{_guid ?? @"NULL"}}} is not a valid asset for {nameof(LooseReference)}: \n\t{_serializationMessage}");
                return null;
            }

            if (string.IsNullOrWhiteSpace(_path))
            {
                IsAssetLoaded = true;
                Asset = null;
                return null;
            }

            IsAssetLoaded = true;
            Asset = Resources.Load<T>(_path);
            if (Asset == null)
                Debug.LogError($"[{nameof(LooseReference)}<{typeof(T).Name}>] Missing asset {_path ?? @"NULL"} {{{_guid ?? @"NULL"}}}");

            return Asset;
        }

        public void UnloadAsset()
        {
            if (!IsAssetLoaded)
                return;

            if (Asset != null)
                Resources.UnloadAsset(Asset);

            IsAssetLoaded = false;
        }

#if UNITY_EDITOR
        public void OnAfterDeserialize() { }

        public void OnBeforeSerialize()
        {
            if (string.IsNullOrWhiteSpace(_guid))
            {
                _path = null;
                _isValidReference = true;
                return;
            }

            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(_guid);

            int pathStart = path.LastIndexOf(@"Resources/");
            if (pathStart < 0)
            {
                _path = null;
                _isValidReference = false;
                _serializationMessage = $"Asset is not located in 'Resources' folder";
                return;
            }

            path = path.Substring(pathStart + @"Resources/".Length);

            int extensionStart = path.LastIndexOf('.');
            if (extensionStart >= 0)
                path = path.Substring(0, extensionStart);

            _path = path;
            _isValidReference = true;
        }
#endif
    }
}
