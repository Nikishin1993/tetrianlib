﻿using System;
using System.Collections.Generic;
using Tetrian.Utility;
using UnityEditor;
using UnityEngine;

namespace Tetrian.Core.ResourceManagement
{
    [CustomPropertyDrawer(typeof(LooseReference), true)]
    public class LooseReferenceDrawer : PropertyDrawer
    {
        private const string GUID_PROPERTY_PATH = @"_guid";
        private const string PATH_PROPERTY_PATH = @"_path";
        private const string IS_VALID_PROPERTY_PATH = @"_isValidReference";
        private const string MESSAGE_PROPERTY_PATH = @"_serializationMessage";

        private static readonly GUIContent _iconContent = EditorGUIUtility.IconContent(@"d_Linked");
        private static readonly float _iconWidth = EditorStyles.label.CalcSize(_iconContent).x;
        private static readonly float _helpBoxHeight = EditorStyles.helpBox.CalcSize(new GUIContent("\n\n")).y;

        private Dictionary<ValueTuple<int, string>, Tuple<LooseReference, SerializedProperty, SerializedProperty, SerializedProperty, SerializedProperty>> _cache = new Dictionary<ValueTuple<int, string>, Tuple<LooseReference, SerializedProperty, SerializedProperty, SerializedProperty, SerializedProperty>>();

        private bool GetProps(SerializedProperty property, out LooseReference looseReference, out SerializedProperty guidProperty, out SerializedProperty pathProperty, out SerializedProperty isValidProperty, out SerializedProperty messageProperty)
        {
            if (_cache.TryGetValue(InspectorUtility.GetSerializedPropertyKey(property), out Tuple<LooseReference, SerializedProperty, SerializedProperty, SerializedProperty, SerializedProperty> refs))
            {
                looseReference = refs.Item1;
                guidProperty = refs.Item2;
                pathProperty = refs.Item3;
                isValidProperty = refs.Item4;
                messageProperty = refs.Item5;
                return true;
            }

            looseReference = (LooseReference)InspectorUtility.GetTargetObjectOfProperty(property);

            if (looseReference == null)
            {
                guidProperty = null;
                pathProperty = null;
                isValidProperty = null;
                messageProperty = null;
                return false;
            }

            guidProperty = property.FindPropertyRelative(GUID_PROPERTY_PATH);
            pathProperty = property.FindPropertyRelative(PATH_PROPERTY_PATH);
            isValidProperty = property.FindPropertyRelative(IS_VALID_PROPERTY_PATH);
            messageProperty = property.FindPropertyRelative(MESSAGE_PROPERTY_PATH);
            _cache[(property.serializedObject.targetObject.GetInstanceID(), property.propertyPath)] = new Tuple<LooseReference, SerializedProperty, SerializedProperty, SerializedProperty, SerializedProperty>(looseReference, guidProperty, pathProperty, isValidProperty, messageProperty);
            return true;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!GetProps(property, out LooseReference looseReference, out SerializedProperty guidProperty, out SerializedProperty pathProperty, out SerializedProperty isValidProperty, out SerializedProperty messageProperty))
                return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + _helpBoxHeight;

            float height = EditorGUIUtility.singleLineHeight;

            if (!isValidProperty.boolValue)
                height += EditorGUIUtility.standardVerticalSpacing + _helpBoxHeight;

#if TETRIAN_LREF_DEBUG
            height += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 2f;
#endif

            return height;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!GetProps(property, out LooseReference looseReference, out SerializedProperty guidProperty, out SerializedProperty pathProperty, out SerializedProperty isValidProperty, out SerializedProperty messageProperty))
            {
                EditorGUI.BeginDisabledGroup(true);
                LooseReferenceField(ref position, label, null, typeof(GameObject));
                EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "\nInspector is not loaded yet\n", MessageType.Warning);
                EditorGUI.EndDisabledGroup();
                return;
            }

            string guid = guidProperty.stringValue;

            UnityEngine.Object oldAsset = null;
            if (!string.IsNullOrWhiteSpace(guid))
                oldAsset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), looseReference.GetAssetType());

            UnityEngine.Object newAsset = LooseReferenceField(ref position, label, oldAsset, looseReference.GetAssetType());
            
            if (newAsset != oldAsset)
            {
                if (newAsset == null)
                    guidProperty.stringValue = null;
                else if (AssetDatabase.IsSubAsset(newAsset))
                    EditorUtility.DisplayDialog(@"Invalid reference", $"Subassets (assets inside assets) is not supported by {nameof(LooseReference)}", @"OK");
                else if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(newAsset, out guid, out long localId))
                    guidProperty.stringValue = guid;
            }

            if (!isValidProperty.boolValue)
                EditorGUI.HelpBox(EditorGUI.IndentedRect(position), messageProperty.stringValue, MessageType.Error);

#if TETRIAN_LREF_DEBUG
            EditorGUI.indentLevel++;
            EditorGUI.BeginDisabledGroup(true);
            EditorGUI.PropertyField(InspectorLayout.VerticalLayout(ref position, EditorGUI.GetPropertyHeight(guidProperty, true), EditorGUIUtility.standardVerticalSpacing), guidProperty);
            EditorGUI.PropertyField(InspectorLayout.VerticalLayout(ref position, EditorGUI.GetPropertyHeight(pathProperty, true), EditorGUIUtility.standardVerticalSpacing), pathProperty);
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
#endif
        }

        private static UnityEngine.Object LooseReferenceField(ref Rect position, GUIContent label, UnityEngine.Object asset, Type assetType)
        {
            Rect mainLineRect = InspectorLayout.VerticalLayout(ref position, EditorGUIUtility.singleLineHeight, EditorGUIUtility.standardVerticalSpacing);
            Rect labelRect = InspectorLayout.HorizontalLayout(ref mainLineRect, EditorGUIUtility.labelWidth - (EditorGUI.indentLevel * 15f), 2f);
            
            EditorGUI.LabelField(labelRect, _iconContent);
            InspectorLayout.HorizontalLayout(ref labelRect, _iconWidth, 2f);
            EditorGUI.LabelField(labelRect, label);
            return EditorGUI.ObjectField(mainLineRect, asset, assetType, false);
        }
    }
}