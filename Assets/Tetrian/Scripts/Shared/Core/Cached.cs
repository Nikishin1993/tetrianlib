﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;

namespace Tetrian.Core
{
    public class Cached<T>
        where T : class
    {
        public event Action<T> ValueChangedEvent;

        public bool HasValue { get; private set; } = false;
        public T Value { get; private set; } = null;

        private Action<T> _onSubscribe = null;
        private Action<T> _onUnsubscribe = null;
        private Action<Cached<T>> _onValueChanged = null;

        public Cached(T value = null, Action<T> onSubscribe = null, Action<T> onUnsubscribe = null, Action<Cached<T>> onValueChanged = null)
        {
            _onSubscribe = onSubscribe;
            _onUnsubscribe = onUnsubscribe;
            _onValueChanged = onValueChanged;
            SetValue(value);
        }

        public bool SetValue(T newValue)
        {
            if (Value == newValue)
                return false;

            if (HasValue)
                _onUnsubscribe?.Invoke(Value);

            Value = newValue;
            HasValue = newValue != null;
            _onValueChanged?.Invoke(this);

            if (HasValue)
                _onSubscribe?.Invoke(Value);

            ValueChangedEvent?.Invoke(Value);
            return true;
        }

        public static implicit operator T(Cached<T> cached) => cached.Value;
    }
}
