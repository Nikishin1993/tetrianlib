﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Core.StateMachine
{
    public struct KeyStatePair<TStateKey, TState>
       where TStateKey : struct//, Enum
       where TState : class, IState
    {
        public TStateKey Key { get; private set; }
        public TState State { get; private set; }

        public KeyStatePair(TStateKey key, TState state)
        {
            Key = key;
            State = state;
        }
    }

    public class StateMachine<TState, TStateKey, TCommand>
        where TState : class, IState
        where TStateKey : struct//, Enum
        where TCommand : struct, Enum
    {
        public Action<TStateKey?, TStateKey?> StateChangedEvent;

        public bool IsRunning { get; private set; } = false;
        public TStateKey? StateKey { get; private set; } = null;
        public TState State { get; private set; } = null;

        private Dictionary<TStateKey, TState> _states = null;
        private Dictionary<ValueTuple<TStateKey, TCommand>, KeyStatePair<TStateKey, TState>> _transitions = null;

        public StateMachine()
        {
            _states = new Dictionary<TStateKey, TState>();
            _transitions = new Dictionary<ValueTuple<TStateKey, TCommand>, KeyStatePair<TStateKey, TState>>();
        }

        public bool HasState(TStateKey key) { return _states.ContainsKey(key); }

        public bool AddState(TStateKey key, TState state)
        {
            if (state == null)
            {
                Debug.LogError($"[{GetType().Name}] Failed to add state: state is NULL");
                return false;
            }

            if (_states.ContainsKey(key))
            {
                Debug.LogError($"[{GetType().Name}] Failed to add state: state with key {key} already exists");
                return false;
            }

            _states.Add(key, state);
            return true;
        }

        public TState GetState(TStateKey key)
        {
            if (_states.TryGetValue(key, out TState state))
                return state;

            return null;
        }

        public bool TryGetState(TStateKey key, out TState state)
        {
            if (_states.TryGetValue(key, out state))
                return true;

            state = default;
            return false;
        }

        private ValueTuple<TStateKey, TCommand> TransitionKey(TStateKey state, TCommand command) { return new ValueTuple<TStateKey, TCommand>(state, command); }

        public bool AddTransition(TStateKey from, TCommand command, TStateKey to)
        {
            if (!TryGetState(to, out TState destination))
            {
                Debug.LogError($"[{GetType().Name}] Failed to add transition: target state not found");
                return false;
            }

            ValueTuple<TStateKey, TCommand> key = TransitionKey(from, command);

            if (_transitions.ContainsKey(key))
            {
                Debug.LogError($"[{GetType().Name}] Failed to add transition: transition with key {key} already exists");
                return false;
            }

            _transitions.Add(key, new KeyStatePair<TStateKey, TState>(to, destination));
            return true;
        }

        private bool SetState(TStateKey? key)
        {
            TState state;

            if (key.HasValue)
            {
                if (!TryGetState(key.Value, out state))
                    return false;
            }
            else
            {
                state = null;
            }

            if (IsRunning)
                State.ExitState();

            IsRunning = key.HasValue;

            StateKey = key;
            State = state;

            if (IsRunning)
                State.EnterState();

            return true;
        }

        public bool Start(TStateKey key)
        {
            if (IsRunning)
            {
                Debug.LogError($"[{GetType().Name}] Failed to start state machine: state machine is already running");
                return false;
            }

            if (SetState(key))
                return true;

            Debug.LogError($"[{GetType().Name}] Failed to start state machine: state with key {key} not found");
            return false;
        }

        public bool Stop()
        {
            if (!IsRunning)
            {
                Debug.LogError($"[{GetType().Name}] Failed to stop state machine: state machine is not running");
                return false;
            }

            SetState(null);
            return true;
        }

        public TStateKey? GetNextStateKey(TCommand command)
        {
            if (!IsRunning)
                return null;

            if (_transitions.TryGetValue(TransitionKey(StateKey.Value, command), out KeyStatePair<TStateKey, TState> pair))
                return pair.Key;

            return null;
        }

        public bool TryGetNextStateKey(TCommand command, out TStateKey key)
        {
            if (IsRunning && _transitions.TryGetValue(TransitionKey(StateKey.Value, command), out KeyStatePair<TStateKey, TState> pair))
            {
                key = pair.Key;
                return true;
            }

            key = default;
            return false;
        }

        public TState GetNextState(TCommand command)
        {
            if (!IsRunning)
                return null;

            if (_transitions.TryGetValue(TransitionKey(StateKey.Value, command), out KeyStatePair<TStateKey, TState> pair))
                return pair.State;

            return null;
        }

        public bool TryGetNextState(TCommand command, out TState state)
        {
            if (IsRunning && _transitions.TryGetValue(TransitionKey(StateKey.Value, command), out KeyStatePair<TStateKey, TState> pair))
            {
                state = pair.State;
                return true;
            }

            state = default;
            return false;
        }

        public bool ApplyCommand(TCommand command)
        {
            if (!IsRunning)
                return false;

            if (_transitions.TryGetValue(TransitionKey(StateKey.Value, command), out KeyStatePair<TStateKey, TState> pair))
                return SetState(pair.Key);

            return false;
        }
    }
}