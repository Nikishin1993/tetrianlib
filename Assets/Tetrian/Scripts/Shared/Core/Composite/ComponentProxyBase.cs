﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Core.Composite
{
    public class ComponentProxyBase<TOwner, TCompBase, TEnum, TComp>// : MonoBehaviour, ICompositionComponent<TOwner, TCompBase, TEnum>
        where TOwner : IComposite<TOwner, TCompBase, TEnum>
        where TCompBase : IComponent<TOwner, TCompBase, TEnum>
        where TComp : class, TCompBase
        where TEnum : struct, Enum
    {
        public bool HasComponent => _component.HasValue;
        public TComp Component => _component.Value;
        public TEnum? ComponentType => HasComponent ? (TEnum?)Component.ComponentType : null;
        public bool IsAcitve => HasComponent && Component.IsActive;

        private Cached<TComp> _component = null;
        private List<IEventProxy<TComp>> _eventProxies = null;

        public ComponentProxyBase()
        {
            _component = new Cached<TComp>(null, Subscribe, Unsubscribe);
            _eventProxies = new List<IEventProxy<TComp>>();
        }

        protected T AddEventProxy<T>(T eventProxy)
            where T : IEventProxy<TComp>
        {
            _eventProxies.Add(eventProxy);
            return eventProxy;
        }

        public void SetComponent(TComp component)
        {
            _component.SetValue(component);
        }

        private void Subscribe(TComp component)
        {
            foreach (IEventProxy<TComp> eventProxy in _eventProxies)
                eventProxy.Subscribe(component);

            OnSubscribe(component);
        }

        private void Unsubscribe(TComp component)
        {
            foreach (IEventProxy<TComp> eventProxy in _eventProxies)
                eventProxy.Unsubscribe(component);

            OnUnsubscribe(component);
        }

        public void Activate() { if (HasComponent) Component.Activate(); }
        public void Deactivate() { if (HasComponent) Component.Deactivate(); }
        public void SetActive(bool isActive) { if (HasComponent) Component.SetActive(isActive); }

        protected virtual void OnSubscribe(TComp component) { }
        protected virtual void OnUnsubscribe(TComp component) { }
    }
}
