﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using Tetrian.Utility;
using UnityEngine;

namespace Tetrian.Core.Composite
{
    public abstract class ComponentBehaviourBase<TOwner, TCompBase, TEnum> : MonoBehaviour, IComponent<TOwner, TCompBase, TEnum>
        where TOwner : IComposite<TOwner, TCompBase, TEnum>
        where TCompBase : IComponent<TOwner, TCompBase, TEnum>
        where TEnum : Enum
    {
        public int ExecutionOrder { get { return Convert.ToInt32(ComponentType); } }
        public abstract TEnum ComponentType { get; }
        public TOwner Owner { get; private set; }
        public bool ActivateOnInitialization => _activateOnInitialization;
        public bool IsActive { get; private set; } = false;

        [SerializeField] private bool _activateOnInitialization = true;

        private bool _isInitialized = false;

        public void Initialize(TOwner owner)
        {
            if (_isInitialized)
            {
                DebugLog.Error(this, $"Component is already initialized");
                return;
            }

            _isInitialized = true;

            Owner = owner;
            OnInitialize();
        }

        public void Activate()
        {
            if (IsActive)
                return;

            IsActive = true;
            OnActivate();
        }

        public void Deactivate()
        {
            if (!IsActive)
                return;

            IsActive = false;
            OnDeactivate();
        }

        public void SetActive(bool isActive)
        {
            if (isActive)
                Activate();
            else
                Deactivate();
        }

        public virtual void OnInitialize() { }
        public virtual void OnActivate() { }
        public virtual void OnDeactivate() { }
    }
}
