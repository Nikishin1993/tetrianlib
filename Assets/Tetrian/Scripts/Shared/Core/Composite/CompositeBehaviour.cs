﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using Tetrian.Utility;
using UnityEngine;

namespace Tetrian.Core.Composite
{
    public abstract class CompositeBehaviour<TOwner, TCompBase, TEnum> : MonoBehaviour, IComposite<TOwner, TCompBase, TEnum>
        where TOwner : IComposite<TOwner, TCompBase, TEnum>
        where TCompBase : IComponent<TOwner, TCompBase, TEnum>
        where TEnum : Enum
    {
        protected abstract TOwner ComponentOwner { get; }

        private bool _isInitialized = false;
        private List<TCompBase> _components = new List<TCompBase>();
        private Dictionary<TEnum, List<TCompBase>> _componentsByTypes = new Dictionary<TEnum, List<TCompBase>>();
        private List<IUpdatableComponent> _updatableComponents = new List<IUpdatableComponent>();
        private List<ILateUpdatableComponent> _lateUpdatableComponents = new List<ILateUpdatableComponent>();
        private List<IFixedUpdatableComponent> _fixedUpdatableComponents = new List<IFixedUpdatableComponent>();

        protected void Initialize(List<TCompBase> components = null, bool autoGatherComponents = false)
        {
            if (_isInitialized)
            {
                DebugLog.Error(this, $"Composition is already initialized");
                return;
            }

            _isInitialized = true;

            if (components == null)
                components = new List<TCompBase>();

            if (autoGatherComponents)
                components.AddRange(GetComponentsInChildren<TCompBase>(true));

            foreach (TCompBase component in components)
                AddComponent_Internal(component);

            OnInitialize();

            _components.Sort((a, b) => CompareComponentsExecutionOrder(a, b));

            foreach (TCompBase component in _components)
                component.Initialize(ComponentOwner);

            OnInitialized();

            foreach (TCompBase component in _components)
                if (component.ActivateOnInitialization)
                    component.Activate();
        }

        private void AddComponent_Internal(TCompBase component)
        {
            if (component == null)
            {
                DebugLog.Error(this, $"Failed to add component to composition: component is NULL");
                return;
            }

            if (_components.Contains(component))
            {
                DebugLog.Error(this, $"Failed to add component to composition: composition already contains this component");
                return;
            }

            _components.Add(component);
            _componentsByTypes.GetOrCreate(component.ComponentType).Add(component);

            if (component is IUpdatableComponent updateable)
                _updatableComponents.Add(updateable);

            if (component is ILateUpdatableComponent lateUpdateable)
                _lateUpdatableComponents.Add(lateUpdateable);

            if (component is IFixedUpdatableComponent fixedUpdateable)
                _fixedUpdatableComponents.Add(fixedUpdateable);
        }

        private void RemoveComponent_Internal(TCompBase component)
        {
            if (component == null)
                return;
            
            _components.Remove(component);
            if (_componentsByTypes.TryGetValue(component.ComponentType, out List<TCompBase> componentGroup))
                componentGroup.Remove(component);

            if (component is IUpdatableComponent updateable)
                _updatableComponents.Remove(updateable);

            if (component is ILateUpdatableComponent lateUpdateable)
                _lateUpdatableComponents.Remove(lateUpdateable);

            if (component is IFixedUpdatableComponent fixedUpdateable)
                _fixedUpdatableComponents.Remove(fixedUpdateable);
        }

        public List<TComp> GetCompositeComponents<TComp>(TEnum componentType)
            where TComp : TCompBase
        {
            if (_componentsByTypes.TryGetValue(componentType, out List<TCompBase> componentGroup))
                return componentGroup.OfType<TComp>();

            return new List<TComp>();
        }

        public List<TCompBase> GetCompositeComponents(TEnum componentType)
        {
            if (_componentsByTypes.TryGetValue(componentType, out List<TCompBase> componentGroup))
                return componentGroup;

            return new List<TCompBase>();
        }

        protected void OnUpdate(float deltaTime)
        {
            foreach (IUpdatableComponent updateable in _updatableComponents)
                if (updateable.IsActive)
                    updateable.OnUpdate(deltaTime);
        }

        protected void OnLateUpdate(float deltaTime)
        {
            foreach (ILateUpdatableComponent lateUpdateable in _lateUpdatableComponents)
                if (lateUpdateable.IsActive)
                    lateUpdateable.OnLateUpdate(deltaTime);
        }

        protected void OnFixedUpdate(float deltaTime)
        {
            foreach (IFixedUpdatableComponent fixedUpdateable in _fixedUpdatableComponents)
                if (fixedUpdateable.IsActive)
                    fixedUpdateable.OnFixedUpdate(deltaTime);
        }

        protected virtual void OnInitialize() { }
        protected virtual void OnInitialized() { }

        private static int CompareComponentsExecutionOrder(IComponent a, IComponent b) { return a.ExecutionOrder.CompareTo(b.ExecutionOrder); }
    }
}
