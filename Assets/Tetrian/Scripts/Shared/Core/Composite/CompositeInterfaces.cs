﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetrian.Core.Composite
{
    public interface IComposite<TOwner, TCompBase, TEnum>
        where TOwner : IComposite<TOwner, TCompBase, TEnum>
        where TCompBase : IComponent<TOwner, TCompBase, TEnum>
        where TEnum : Enum
    {
        List<TComp> GetCompositeComponents<TComp>(TEnum componentType) where TComp : TCompBase;
        List<TCompBase> GetCompositeComponents(TEnum componentType);
    }

    public interface IComponent
    {
        int ExecutionOrder { get; }
        bool IsActive { get; }
    }

    public interface IComponent<TOwner, TCompBase, TEnum> : IComponent
        where TOwner : IComposite<TOwner, TCompBase, TEnum>
        where TCompBase : IComponent<TOwner, TCompBase, TEnum>
        where TEnum : Enum
    {
        TEnum ComponentType { get; }
        TOwner Owner { get; }
        bool ActivateOnInitialization { get; }

        void Initialize(TOwner owner);
        void Activate();
        void Deactivate();
        void SetActive(bool isActive);
    }


    public interface IUpdatableComponent : IComponent
    {
        void OnUpdate(float deltaTime);
    }

    public interface ILateUpdatableComponent : IComponent
    {
        void OnLateUpdate(float deltaTime);
    }

    public interface IFixedUpdatableComponent : IComponent
    {
        void OnFixedUpdate(float deltaTime);
    }
}
