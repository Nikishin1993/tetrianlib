﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;

namespace Tetrian.Core
{
    public class ActionEventProxy<TO> : EventProxy<TO, Action>
    {
        public event Action Event;
        protected override Action HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action> subscribeDelegate, Action<TO, Action> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler() { Event?.Invoke(); }
    }

    public class ActionEventProxy<TO, T1> : EventProxy<TO, Action<T1>>
    {
        public event Action<T1> Event;
        protected override Action<T1> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1>> subscribeDelegate, Action<TO, Action<T1>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1) { Event?.Invoke(arg1); }
    }

    public class ActionEventProxy<TO, T1, T2> : EventProxy<TO, Action<T1, T2>>
    {
        public event Action<T1, T2> Event;
        protected override Action<T1, T2> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2>> subscribeDelegate, Action<TO, Action<T1, T2>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2) { Event?.Invoke(arg1, arg2); }
    }

    public class ActionEventProxy<TO, T1, T2, T3> : EventProxy<TO, Action<T1, T2, T3>>
    {
        public event Action<T1, T2, T3> Event;
        protected override Action<T1, T2, T3> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3>> subscribeDelegate, Action<TO, Action<T1, T2, T3>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3) { Event?.Invoke(arg1, arg2, arg3); }
    }

    public class ActionEventProxy<TO, T1, T2, T3, T4> : EventProxy<TO, Action<T1, T2, T3, T4>>
    {
        public event Action<T1, T2, T3, T4> Event;
        protected override Action<T1, T2, T3, T4> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3, T4>> subscribeDelegate, Action<TO, Action<T1, T2, T3, T4>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3, T4 arg4) { Event?.Invoke(arg1, arg2, arg3, arg4); }
    }

    public class ActionEventProxy<TO, T1, T2, T3, T4, T5> : EventProxy<TO, Action<T1, T2, T3, T4, T5>>
    {
        public event Action<T1, T2, T3, T4, T5> Event;
        protected override Action<T1, T2, T3, T4, T5> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3, T4, T5>> subscribeDelegate, Action<TO, Action<T1, T2, T3, T4, T5>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5) { Event?.Invoke(arg1, arg2, arg3, arg4, arg5); }
    }

    public class ActionEventProxy<TO, T1, T2, T3, T4, T5, T6> : EventProxy<TO, Action<T1, T2, T3, T4, T5, T6>>
    {
        public event Action<T1, T2, T3, T4, T5, T6> Event;
        protected override Action<T1, T2, T3, T4, T5, T6> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3, T4, T5, T6>> subscribeDelegate, Action<TO, Action<T1, T2, T3, T4, T5, T6>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6) { Event?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6); }
    }

    public class ActionEventProxy<TO, T1, T2, T3, T4, T5, T6, T7> : EventProxy<TO, Action<T1, T2, T3, T4, T5, T6, T7>>
    {
        public event Action<T1, T2, T3, T4, T5, T6, T7> Event;
        protected override Action<T1, T2, T3, T4, T5, T6, T7> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3, T4, T5, T6, T7>> subscribeDelegate, Action<TO, Action<T1, T2, T3, T4, T5, T6, T7>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7) { Event?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7); }
    }

    public class ActionEventProxy<TO, T1, T2, T3, T4, T5, T6, T7, T8> : EventProxy<TO, Action<T1, T2, T3, T4, T5, T6, T7, T8>>
    {
        public event Action<T1, T2, T3, T4, T5, T6, T7, T8> Event;
        protected override Action<T1, T2, T3, T4, T5, T6, T7, T8> HandlerDelegate => Handler;

        public ActionEventProxy(Action<TO, Action<T1, T2, T3, T4, T5, T6, T7, T8>> subscribeDelegate, Action<TO, Action<T1, T2, T3, T4, T5, T6, T7, T8>> unsubscribeDelegate) : base(subscribeDelegate, unsubscribeDelegate) { }

        private void Handler(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8) { Event?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8); }
    }
}
