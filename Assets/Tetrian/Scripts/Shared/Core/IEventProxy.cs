﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2021 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;

namespace Tetrian.Core
{
    public interface IEventProxy<T>
    {
        void Subscribe(T obj);
        void Unsubscribe(T obj);
    }

    public abstract class EventProxy<TO, TH> : IEventProxy<TO>
        where TH : Delegate
    {
        protected abstract TH HandlerDelegate { get; }

        private readonly Action<TO, TH> _subscribeDelegate;
        private readonly Action<TO, TH> _unsubscribeDelegate;

        public EventProxy(Action<TO, TH> subscribeDelegate, Action<TO, TH> unsubscribeDelegate)
        {
            _subscribeDelegate = subscribeDelegate;
            _unsubscribeDelegate = unsubscribeDelegate;
        }

        public void Subscribe(TO obj) { _subscribeDelegate.Invoke(obj, HandlerDelegate); }
        public void Unsubscribe(TO obj) { _unsubscribeDelegate.Invoke(obj, HandlerDelegate); }
    }
}
