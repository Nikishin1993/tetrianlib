// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Tetrian.Shared.InventorySamples
{
    public class SampleInventoryUiSlot : MonoBehaviour
    {
        [SerializeField] private GameObject _filledState = null;
        [SerializeField] private GameObject _emptyState = null;
        [SerializeField] private Image _itemIconImage = null;
        [SerializeField] private TMP_Text _itemCountText = null;

        public void SetItems(IReadOnlyCollection<SampleInventoryItem> items)
        {
            bool hasItem = items != null && items.Count > 0;

            _filledState.SetActive(hasItem);
            _emptyState.SetActive(!hasItem);

            if (!hasItem)
                return;

            foreach (SampleInventoryItem item in items)
            {
                _itemIconImage.sprite = item.Icon;
                _itemCountText.text = items.Count.ToString();
                break;
            }
        }
    }
}
