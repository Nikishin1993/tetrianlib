// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using Tetrian.Shared.Inventory.Abstractions;
using UnityEngine;

namespace Tetrian.Shared.InventorySamples
{
    [CreateAssetMenu(menuName = @"Tetrian/Inventory Samples/New Inventory Item", fileName = @"NewInventoryItem")]
    public class SampleInventoryItem : ScriptableObject, IInventoryItem
    {
        public int Id => GetInstanceID();
        public int StackSize => _stackSize;
        public float Weight => _weight;
        public Sprite Icon => _icon;

        [SerializeField] private string _name = @"Item";
        [SerializeField] private int _stackSize = 1;
        [SerializeField] private float _weight = 1f;
        [SerializeField] private Sprite _icon = null;
    }
}
