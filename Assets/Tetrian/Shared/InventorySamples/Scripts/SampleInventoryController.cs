// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections.Generic;
using Tetrian.Shared.Inventory.Implementations;
using UnityEngine;

namespace Tetrian.Shared.InventorySamples
{
    public class SampleInventoryController : MonoBehaviour
    {
        [Serializable]
        public class SlotContentsInfo
        {
            public int Slot;
            public SampleInventoryItem Item;
            public int Count;
        }

        public bool IsOverEncumbred => _inventory.IsOverEncumbred;
        public float MaxWeight => _inventory.MaxWeight;
        public float CurrentWeight => _inventory.CurrentWeight;
        public int SlotsCount => _inventory.SlotsCount;

        [SerializeField] private int _slotsCount = 10;
        [SerializeField] private float _maxWeight = 150f;
        [SerializeField] private List<SlotContentsInfo> _initialContents = null;

        private InventoryController<SampleInventoryItem> _inventory = null;

        private void Awake()
        {
            _inventory = new InventoryController<SampleInventoryItem>(_slotsCount, _maxWeight);

            foreach (SlotContentsInfo slotContent in _initialContents)
            {
                for (int i = 0; i < slotContent.Count; i++)
                    _inventory.AddItem(slotContent.Slot, slotContent.Item);
            }
        }

        public bool AddItemToAnySlot(SampleInventoryItem item) => _inventory.AddItemToAnySlot(item);
        public bool AddItem(int slot, SampleInventoryItem item) => _inventory.AddItem(slot, item);
        public bool RemoveItem(int slot, out SampleInventoryItem item) => _inventory.RemoveItem(slot, out item);
        public IReadOnlyCollection<SampleInventoryItem> GetItems(int slot) => _inventory.GetItems(slot);
    }
}
