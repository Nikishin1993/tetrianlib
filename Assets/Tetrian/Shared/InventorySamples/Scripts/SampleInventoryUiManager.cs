// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Tetrian.Shared.InventorySamples
{
    public class SampleInventoryUiManager : MonoBehaviour
    {
        [SerializeField] private SampleInventoryUiPanel _inventoryPanel = null;

        [SerializeField] private SampleInventoryItem _appleItem = null;
        [SerializeField] private SampleInventoryItem _orangeItem = null;
        [SerializeField] private SampleInventoryItem _bananaItem = null;

        [SerializeField] private Button _addAppleButton = null;
        [SerializeField] private Button _addOrangeButton = null;
        [SerializeField] private Button _addBananaButton = null;
        [SerializeField] private Button _removeFirstButton = null;
        [SerializeField] private Button _removeLastButton = null;


        private void Start()
        {
            _inventoryPanel.Initialize();

            _addAppleButton.onClick.AddListener(OnAddAppleButtonClicked);
            _addOrangeButton.onClick.AddListener(OnAddOrangeButtonClicked);
            _addBananaButton.onClick.AddListener(OnAddBananaButtonClicked);
            _removeFirstButton.onClick.AddListener(OnRemoveFirstButtonClicked);
            _removeLastButton.onClick.AddListener(OnRemoveLastButtonClicked);
        }

        private void OnAddAppleButtonClicked()
        {
            _inventoryPanel.Inventory.AddItemToAnySlot(_appleItem);
            _inventoryPanel.Refresh();
        }

        private void OnAddOrangeButtonClicked()
        {
            _inventoryPanel.Inventory.AddItemToAnySlot(_orangeItem);
            _inventoryPanel.Refresh();
        }

        private void OnAddBananaButtonClicked()
        {
            _inventoryPanel.Inventory.AddItemToAnySlot(_bananaItem);
            _inventoryPanel.Refresh();
        }

        private void OnRemoveFirstButtonClicked()
        {
            for (int i = 0; i < _inventoryPanel.Inventory.SlotsCount; i++)
            {
                IReadOnlyCollection<SampleInventoryItem> items = _inventoryPanel.Inventory.GetItems(i);

                if (items != null && items.Count > 0)
                {
                    _inventoryPanel.Inventory.RemoveItem(i, out SampleInventoryItem item);
                    _inventoryPanel.Refresh();
                    return;
                }
            }
        }

        private void OnRemoveLastButtonClicked()
        {
            for (int i = 0; i < _inventoryPanel.Inventory.SlotsCount; i++)
            {
                int j = _inventoryPanel.Inventory.SlotsCount - 1 - i;

                IReadOnlyCollection<SampleInventoryItem> items = _inventoryPanel.Inventory.GetItems(j);

                if (items != null && items.Count > 0)
                {
                    _inventoryPanel.Inventory.RemoveItem(j, out SampleInventoryItem item);
                    _inventoryPanel.Refresh();
                    return;
                }
            }
        }
    }
}
