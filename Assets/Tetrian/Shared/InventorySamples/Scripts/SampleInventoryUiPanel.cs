// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Tetrian.Shared.InventorySamples
{
    public class SampleInventoryUiPanel : MonoBehaviour
    {
        public SampleInventoryController Inventory => _inventory;

        [SerializeField] private SampleInventoryController _inventory = null;
        [SerializeField] private Transform _slotsContainer = null;
        [SerializeField] private SampleInventoryUiSlot _slotPrefab = null;
        [SerializeField] private TMP_Text _currentWeightText = null;
        [SerializeField] private TMP_Text _maxWeightText = null;

        private SampleInventoryUiSlot[] _slots = null;

        public void Initialize()
        {
            _slots = new SampleInventoryUiSlot[_inventory.SlotsCount];

            for (int i = 0; i < _inventory.SlotsCount; i++)
            {
                SampleInventoryUiSlot slot = GameObject.Instantiate(_slotPrefab, _slotsContainer);
                slot.gameObject.SetActive(true);
                _slots[i] = slot;
            }

            Refresh();
        }

        public void Refresh()
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                _slots[i].SetItems(_inventory.GetItems(i));
            }

            _currentWeightText.text = _inventory.CurrentWeight.ToString();
            _maxWeightText.text = _inventory.MaxWeight.ToString();
        }
    }
}
