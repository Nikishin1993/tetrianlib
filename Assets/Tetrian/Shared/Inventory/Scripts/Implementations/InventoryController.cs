﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections.Generic;
using Tetrian.Shared.Inventory.Abstractions;

namespace Tetrian.Shared.Inventory.Implementations
{
    public class InventoryController<T>
        where T : class, IInventoryItem
    {
        public bool IsOverEncumbred => MaxWeight < CurrentWeight;
        public float MaxWeight => _maxWeight;
        public float CurrentWeight => _weightCounter.CurrentWeight;

        public int SlotsCount => _storage.SlotsCount;

        private readonly IInventoryStorage<T> _storage;
        private readonly IInventoryStackProvider<T> _stackProvider;
        private readonly IInventoryWeightCounter<T> _weightCounter;

        private float _maxWeight = 0f;

        public InventoryController(int slotsCount, float maxWeight)
        {
            _storage = new InventoryStorage<T>(slotsCount);
            _stackProvider = new InventoryStackProvider<T>();
            _weightCounter = new InventoryWeightCounter<T>(_storage);

            _maxWeight = maxWeight;
        }

        public bool AddItemToAnySlot(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            int stackSize = _stackProvider.GetStackSize(item);

            for (int i = 0; i < _storage.SlotsCount; i++)
            {
                if (AddItem_Internal(i, item, stackSize))
                    return true;
            }

            return false;
        }

        public bool AddItem(int slot, T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            return AddItem_Internal(slot, item, _stackProvider.GetStackSize(item));
        }

        private bool AddItem_Internal(int slot, T item, int stackSize)
        {
            T storedItem = _storage.GetItem(slot);

            if (storedItem != null)
            {
                if (!_stackProvider.CanStack(item, storedItem))
                    return false;

                if (stackSize <= _storage.GetItemsCount(slot))
                    return false;
            }

            _storage.AddItem(slot, item);
            _weightCounter.OnItemAdded(item);
            return true;
        }

        public bool RemoveItem(int slot, out T item)
        {
            if (_storage.RemoveItem(slot, out item))
            {
                _weightCounter.OnItemRemoved(item);
                return true;
            }

            item = null;
            return false;
        }

        public IReadOnlyCollection<T> GetItems(int slot)
        {
            return _storage.GetItems(slot);
        }
    }
}
