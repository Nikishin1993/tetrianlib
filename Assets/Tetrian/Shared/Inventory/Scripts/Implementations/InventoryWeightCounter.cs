﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System.Collections.Generic;
using Tetrian.Shared.Inventory.Abstractions;

namespace Tetrian.Shared.Inventory.Implementations
{
    public class InventoryWeightCounter<T> : IInventoryWeightCounter<T>
        where T : class, IInventoryItem
    {
        public float CurrentWeight => _currentWeight;

        private readonly IInventoryStorage<T> _storage;

        private float _currentWeight = 0f;

        public InventoryWeightCounter(IInventoryStorage<T> storage)
        {
            _storage = storage;

            Refresh();
        }

        public void OnItemAdded(T item)
        {
            _currentWeight += item.Weight;
        }

        public void OnItemRemoved(T item)
        {
            _currentWeight -= item.Weight;
        }

        public void Refresh()
        {
            _currentWeight = 0f;

            for (int i = 0; i < _storage.SlotsCount; i++)
            {
                IReadOnlyCollection<T> slotItems = _storage.GetItems(i);

                if (slotItems == null)
                    continue;

                foreach (T item in slotItems)
                    _currentWeight += item.Weight;
            }
        }
    }
}
