// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using System.Collections.Generic;
using Tetrian.Shared.Inventory.Abstractions;

namespace Tetrian.Shared.Inventory.Implementations
{
    public class InventoryStorage<T> : IInventoryStorage<T>
        where T : class, IInventoryItem
    {
        public int SlotsCount => _slots.Length;

        private Stack<T>[] _slots = null;

        public InventoryStorage(int slotsCount)
        {
            _slots = new Stack<T>[slotsCount];

            for (int i = 0; i < _slots.Length; i++)
                _slots[i] = new Stack<T>();
        }

        public void AddItem(int slot, T item)
        {
            EnsureSlotIsValid(slot, nameof(slot));
            EnsureItemIsValid(item, nameof(item));

            _slots[slot].Push(item);
        }

        public bool RemoveItem(int slot, out T item)
        {
            EnsureSlotIsValid(slot, nameof(slot));

            return _slots[slot].TryPop(out item);
        }

        public T GetItem(int slot)
        {
            EnsureSlotIsValid(slot, nameof(slot));

            if (!_slots[slot].TryPeek(out T item))
                return null;

            return item;
        }

        public IReadOnlyCollection<T> GetItems(int slot)
        {
            EnsureSlotIsValid(slot, nameof(slot));

            return _slots[slot];
        }

        public int GetItemsCount(int slot)
        {
            EnsureSlotIsValid(slot, nameof(slot));

            return _slots[slot].Count;
        }

        private void EnsureSlotIsValid(int slot, string paramName)
        {
            if (!(0 <= slot && slot < _slots.Length))
                throw new ArgumentOutOfRangeException(paramName);
        }

        private void EnsureItemIsValid(T item, string paramName)
        {
            if (item == null)
                throw new ArgumentNullException(paramName);
        }
    }
}
