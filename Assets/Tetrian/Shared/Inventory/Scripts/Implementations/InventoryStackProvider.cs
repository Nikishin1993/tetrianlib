﻿// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// * Copyright (c) 2019 - 2023 Artem Nikishin                                                *
// * This work is licensed under the Creative Commons Attribution 4.0 International License. *
// * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/       *
// * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.        *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

using System;
using Tetrian.Shared.Inventory.Abstractions;

namespace Tetrian.Shared.Inventory.Implementations
{
    public class InventoryStackProvider<T> : IInventoryStackProvider<T>
    where T : class, IInventoryItem
    {
        public bool CanStack(T itemA, T itemB)
        {
            if (itemA == null)
                throw new ArgumentNullException(nameof(itemA));

            if (itemB == null)
                throw new ArgumentNullException(nameof(itemB));

            return itemA.Id == itemB.Id;
        }

        public int GetStackSize(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            return item.StackSize;
        }
    }
}
